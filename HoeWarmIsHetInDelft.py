"""This script navigates to http://www.weerindelft.nl/, reads the temperature information and prints it to
stdout (rounded to degrees Celsius)"""

import re

from contextlib import contextmanager
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

SOURCE = 'http://www.weerindelft.nl/'
TEMP_PATT = r'([\d+.]+)°C'


@contextmanager
def chrome_driver() -> webdriver:
    """Context manager that instantiates a headless chrome browser. It then yields the instance of webdriver to the
    caller and cleans up once the calling context is done. This ensures that the driver is always closed, even when
    there's an exception in the caller context.

    :return: Instance of webdriver after accessing the web page of interest
    :rtype: WebDriver
    """
    options = Options()
    options.add_argument('--headless')
    driver_instance = webdriver.Chrome(options=options)
    try:
        yield driver_instance
    finally:
        driver_instance.quit()


def parse_temperature_string(temp_string: str) -> int:
    """Parses a given temperature string and returns the value rounded to nearest integer

    :param temp_string: The temperature as a string (e.g. 22.3°C)
    :type temp_string: str
    :return: The temperature rounded to nearest integer
    :rtype: int
    :raises: AttributeError if re.search fails
    """
    parsed_temp = re.search(TEMP_PATT, temp_string)
    rounded_temp = round(float(parsed_temp.group(1)))
    return rounded_temp


def main():
    """Reads and parses the temperature displayed at 'http://www.weerindelft.nl/' and prints it to stdout
    """
    with chrome_driver() as driver:
        # Navigate to the web page using the driver
        driver.get(SOURCE)
        driver.implicitly_wait(15)

        # Find the iframe and the element that contains the temperature information (id='ajaxtemp')
        iframe_of_interest = driver.find_element_by_id('ifrm_3')
        driver.switch_to.frame(iframe_of_interest)

        # Extract the text from the element
        raw_temp = driver.find_element_by_id('ajaxtemp').text
        driver.switch_to.default_content()

        # Parse and print the temperature
        print(f"{parse_temperature_string(raw_temp)} degrees Celsius")


if __name__ == '__main__':
    main()
